terraform {
    required_version = ">= v0.12.6"
    backend "s3" {
        encrypt = true
        bucket = "tfstate-847991"
        key = "backend-lock.tfstate"
    }
}

provider "aws" {}

resource "aws_dynamodb_table" "tfstate-lock" {
  name = "tfstate-lock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
 
  attribute {
    name = "LockID"
    type = "S"
  } 
}
